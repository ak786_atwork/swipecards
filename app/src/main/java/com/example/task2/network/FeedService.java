package com.example.task2.network;


import com.example.task2.model.Feed;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface FeedService {
    @GET("fjaqJ")
    Observable<List<Feed>> getAllFeeds();
}
