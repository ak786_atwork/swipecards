package com.example.task2.network;

import com.example.task2.converter.MyConverter;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

public class FeedApiClient {

    private static final String BASE_URL = "https://git.io/";
    private static Retrofit retrofit;
    private static FeedService feedService;

    private static void init(){

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60,TimeUnit.SECONDS)
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(new MyConverter())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        feedService = retrofit.create(FeedService.class);

    }

    public static FeedService getFeedService(){
        if (feedService == null)
            init();
        return feedService;
    }

}
