package com.example.task2.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.task2.model.Feed;
import com.example.task2.repository.FeedRepository;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class FeedViewModel extends ViewModel {

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    private MutableLiveData<List<Feed>> feedMutableData = new MutableLiveData<>();
    private FeedRepository feedRepository = new FeedRepository();
    public MutableLiveData<List<Feed>> getFeedMutableData(){
        return feedMutableData;
    }

    public void fetchFeeds(){
        Disposable repoDisposable = feedRepository.fetchFeeds()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        (data) -> feedMutableData.postValue(data)
                );
        compositeDisposable.add(repoDisposable);
    }

    @Override
    protected void onCleared() {
        compositeDisposable.clear();
        super.onCleared();
    }
}
