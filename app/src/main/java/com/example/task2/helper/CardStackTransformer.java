package com.example.task2.helper;

import android.view.View;

import androidx.viewpager.widget.ViewPager;

/*
* To add page animations
* */
public class CardStackTransformer implements ViewPager.PageTransformer {
    @Override
    public void transformPage(View page, float position) {
        if (position >= 0) {
            page.setScaleX(0.99f - 0.02f * position);
            page.setScaleY(0.99f);
            page.setTranslationX(-page.getWidth() * position);
            page.setTranslationY(30 * position);
        }

    }
}