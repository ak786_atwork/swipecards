package com.example.task2.adapter;


import android.util.Log;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.task2.model.Feed;
import com.example.task2.fragment.CardStackFragment;

import java.util.List;

public class CardStackAdapter extends FragmentStatePagerAdapter {

    private static final String TAG = "CardStackAdapter";

    private List<Feed> mList;
    public CardStackAdapter(FragmentManager fm, List<Feed> list) {
        super(fm);
        this.mList = list;
    }
    public void setList(List<Feed> list) {
        this.mList = list;
        notifyDataSetChanged();
    }


    @Override
    public Fragment getItem(int position) {
        return new CardStackFragment(mList.get(position));
    }

    @Override
    public int getCount() {
//        Log.d(TAG,"size = "+mList.size());
        return mList.size();
    }
}
