package com.example.task2.repository;

import com.example.task2.model.Feed;
import com.example.task2.network.FeedApiClient;

import java.util.List;

import io.reactivex.Observable;

/*
* Fetching data from network
* */
public class RemoteFeedSource implements FeedDataSourceInterface {
    @Override
    public Observable<List<Feed>> fetchFeeds() {
        return FeedApiClient.getFeedService().getAllFeeds();
    }
}
