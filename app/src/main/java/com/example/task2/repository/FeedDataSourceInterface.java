package com.example.task2.repository;


import com.example.task2.model.Feed;
import java.util.List;
import io.reactivex.Observable;


/*
* Common interface that all other data sources will implement
* */
public interface FeedDataSourceInterface {
    Observable<List<Feed>> fetchFeeds();
}
