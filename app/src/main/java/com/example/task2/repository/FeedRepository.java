package com.example.task2.repository;

import com.example.task2.model.Feed;

import java.util.List;

import io.reactivex.Observable;

public class FeedRepository {

    private static final String TAG = "FeedRepository";
    private RemoteFeedSource remoteFeedSource;


    public FeedRepository() {
        this.remoteFeedSource = new RemoteFeedSource();
    }


    public Observable<List<Feed>> fetchFeeds() {
        return remoteFeedSource.fetchFeeds()
                .onErrorResumeNext(Observable.empty());
    }


}
