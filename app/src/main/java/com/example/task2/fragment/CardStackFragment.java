package com.example.task2.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.task2.R;
import com.example.task2.model.Feed;

public class CardStackFragment extends Fragment {

    private Feed feed;

    public CardStackFragment(Feed feed){
        this.feed = feed;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_card,null);
        TextView textView= view.findViewById(R.id.tv);
        textView.setText(feed.text);
        return view;
    }
}
