package com.example.task2.converter;

import android.util.Log;

import com.example.task2.model.Feed;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Retrofit;

/*
* Custom converter to handle the '\' in response
* */

public class MyConverter extends Converter.Factory {
    @Override
    public Converter<ResponseBody, ?> responseBodyConverter(Type type, Annotation[] annotations, Retrofit retrofit) {
        return (Converter<ResponseBody, Object>) value -> {
            String responseString = value.string();
            String jsonResponse = responseString.substring(1);
            Log.d("data",responseString);
            Gson gson = new Gson();
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(jsonResponse);
                return gson.fromJson( jsonObject.getJSONArray("data").toString(), new TypeToken<List<Feed>>() {}.getType());
            } catch (JSONException e) {
                e.printStackTrace();
                return "";
            }

        };
    }
}
