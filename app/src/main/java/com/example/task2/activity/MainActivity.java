package com.example.task2.activity;


import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;

import com.example.task2.R;
import com.example.task2.adapter.CardStackAdapter;
import com.example.task2.helper.CardStackTransformer;
import com.example.task2.model.Feed;
import com.example.task2.viewmodel.FeedViewModel;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ViewPager mPager;
    private CardStackAdapter mAdapter;
    private FeedViewModel mViewModel;
    private List<Feed> mFeedList;
    private int mPageNumber = 0;
    private int mTotalPages = 0;
    private TextView mTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mViewModel = ViewModelProviders.of(this).get(FeedViewModel.class);
        mFeedList = new ArrayList<>();

        mTextView = findViewById(R.id.item_number_tv);

        mPager = findViewById(R.id.viewPager);
        mAdapter = new CardStackAdapter(getSupportFragmentManager(), mFeedList);
        mPager.setPageTransformer(true, new CardStackTransformer());
        mPager.setOffscreenPageLimit(5);
        mPager.setAdapter(mAdapter);

        attachObserver();
        mViewModel.fetchFeeds();

        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mPageNumber = position+1;
                updatePageNumber();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    private void updatePageNumber(){
        mTextView.setText(mPageNumber+"/"+mTotalPages);
    }

    /*
    * observer attached to listen for update in feeds
    */
    private void attachObserver() {

        mViewModel.getFeedMutableData().observe(this, feeds -> {
            mFeedList = feeds;
            mTotalPages = mFeedList.size();
            mAdapter.setList(feeds);
            mPageNumber = 1;
            updatePageNumber();
        });
    }

}
